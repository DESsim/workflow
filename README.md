## What is this module for ?
This module orchestrates the different calculations involved in a smartgrid simulation :
- Creating a geometry file from SIG data.
- Simulating the power demand of buildings given this geometry, various hypothesis and a weather file (with climelioth).
- To do : simulating the power balance (with pypsa).
- To do : extracting performance metrics (carbon, energy, costs...).

It also manages results versioning : 
- It runs only calculations when their inputs have changed.
- It creates new results files when inputs have changed, so you can do sensitivity tests and scenarios.

## Installation instructions
You need to install luigi :
```
pip install luigi
``` 

or if you use Anaconda

```
 conda install -c anaconda luigi 
``` 

**You must indicate where the needed python modules and executables are located** :
Copy paste the data/config_template.json to create a data/config.json file, and adapt the paths for your machine (the config.json file is not tracked by git).

**You must add the path to this module to your python path for it to work :**
```
```
set PYTHONPATH=%PYTHONPATH%;path/to/workflow/python

## Examples
Run luigi's central scheduler to get a dashboard of running and completed tasks (with error messages in case of failure), at http://localhost:8082/ :
```
luigid
```

To run Climelioth weather generator with a given EPW weather file or RT2012 zone:
```
luigi --module workflow getWeather
```
You can add an additional parameter:
```
--weather Paris_Montsouris-hour.epw
```
to specify a custom EPW weather file or RT2012 zone. Otherwise, the H1a zone is chosen by default.



To run Climelioth (power demand) with the geometry of Bruneseau version #1 and the set of hypothesis version #1, run :
```
luigi --module workflow RunClimelioth --geometry-version 3 --std-hyps-version 1 --ratios ope_H1a_2017
```
You can add an additional parameter:
```
--weather Paris_Montsouris-hour.epw
```
to specify a custom weather scenario. Otherwise, the H1a zone is chosen by default.



To run Solar (PV yield) with the geometry of Bruneseau version #1 and the set of solar parameters version #1, run :
```
luigi --module workflow RunSolar --geometry-version 3 --solar-parameters-version 1
```
You can add an additional parameter:
```
--weather Paris_Montsouris-hour.epw
```
to specify a custom weather scenario. Otherwise, the H1a zone is chosen by default.



To run Smartgrid (power balance between buildings and energy networks) with the geometry of Bruneseau version #1 and the set of hypothesis version #1, run :
```
luigi --module workflow RunSmartgrid --geometry-version 3 --std-hyps-version 1 --phasing-version 1 --solar-parameters-version 1 
```
You can add an additional parameter:
```
--weather Paris_Montsouris-hour.epw
```
to specify a custom weather scenario. Otherwise, the H1a zone is chosen by default.

You can add an additional parameter:
```
--network reseau-test.gpkg
```
to specify a network geometry.