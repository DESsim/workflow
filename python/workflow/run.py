import luigi
import os
import json
import subprocess
import shutil
import uuid
import datetime

class Config(luigi.Config):
    # Find the path of the script
    script_path = os.path.abspath(__file__)
    module_path = os.path.dirname(os.path.dirname(os.path.dirname(script_path)))

    with open(os.path.join(module_path, 'data', 'config.json'), 'r') as file:
        config_paths = json.load(file)


def output_exists(data_path, inputs, input_versions, name=""):

    # List the output versions already existing
    output_versions = os.listdir(os.path.join(data_path, 'out'))
    exists = False

    # Search for a version with equal inputs
    for o in output_versions:

        with open(os.path.join(data_path, 'out', o, 'inputs.json'), 'r') as file:
            versions = json.load(file)

            if versions == input_versions:
                exists = True
                version = o

    # Bump the version number if no existing output has been found
    if exists is False:
        if len(name) > 0:
            version = name
        else:
            version = str(uuid.uuid1())
            #version = "+".join(list(input_versions.values()))
            #version = str(len(output_versions) + 1)

    # If an output already exists, check if task inputs still exist
    else:
        for k, v in inputs.items():
            # If any of the inputs folder is missing, remove the folder
            if os.path.isdir(v.input()['output_folder'].path) == False:
                print("Warning: inputs folder missing")
                # shutil.rmtree(os.path.join(data_path, 'out', o))
                # exists = False
                # break

    return exists, version



class CreateOutputDirectory(luigi.Task):

    path = luigi.Parameter()
    input_versions = luigi.DictParameter()

    def output(self):
        return luigi.LocalTarget(self.path)

    def run(self):

        os.mkdir(self.path)

        with open(os.path.join(self.path, 'inputs.json'), 'w') as file:
            json.dump(self.input_versions.get_wrapped(), file)


class BuildGeometry(luigi.Task):

    geometry_version = luigi.Parameter()
    data_path = os.path.join(Config().config_paths['project_data'], 'consommation', 'geometrie')

    def requires(self):

        # Tasks inputs dict
        inputs = {}

        # Versions of the inputs
        input_versions = {
            'geometry': self.geometry_version
        }

        # Test if an output already exists for this set of inputs
        exist, version = output_exists(self.data_path, inputs, input_versions)

        # Point to the existing folder if the output already exists or create a folder
        output_path = os.path.join(self.data_path, 'out', version)

        # Add the output folder creation as input task
        inputs['output_folder'] = CreateOutputDirectory(output_path, input_versions)

        return inputs

    def output(self):
        return luigi.LocalTarget(os.path.join(self.input()['output_folder'].path, "buildings.json"))

    def run(self):

        subprocess.call([
            'python',
            Config().config_paths['geometry'],
            '-v', os.path.join(self.data_path, 'in', self.geometry_version, 'volumes.gpkg'),
            '-m', os.path.join(self.data_path, 'in', self.geometry_version, 'masks.gpkg'),
            '-p', os.path.join(self.data_path, 'in', self.geometry_version, 'programme.xlsx'),
            '-o', self.output().path
        ])

class getWeather(luigi.Task):

    weather_name = luigi.Parameter(default="H1a")
    data_path = os.path.join(Config().config_paths['project_data'], 'meteo')

    def requires(self):

        # Tasks inputs dict
        inputs = {}

        # Versions of the inputs
        input_versions = {
            'weather-name': self.weather_name
        }
        
        # Test if an output already exists for this set of inputs
        exist, version = output_exists(self.data_path, inputs, input_versions, self.weather_name)

        # Point to the existing folder if the output already exists or create a folder
        output_path = os.path.join(self.data_path, 'out', version)

        # Add the output folder creation as input task
        inputs['output_folder'] = CreateOutputDirectory(output_path, input_versions)

        return inputs

    def output(self):
        return luigi.LocalTarget(os.path.join(self.input()['output_folder'].path, self.weather_name+".csv"))

    def run(self):
        weather_input = self.weather_name
        if weather_input[-4:] == ".csv" or weather_input[-4:] == ".epw":
            weather = os.path.join(self.data_path, 'in', weather_input)
        else:
            weather = weather_input

        subprocess.call([
            'python',
            Config().config_paths['weather-import'],
            '-w', weather,
            '-o', self.output().path
        ])


class RunClimelioth(luigi.Task):

    std_hyps_version = luigi.Parameter()
    geometry_version = luigi.Parameter()
    weather_input = luigi.Parameter(default="H1a")
    ratios = luigi.Parameter(default="")
    ProgrammeScaling = luigi.Parameter(default="False")
    STDreplacement = luigi.Parameter(default="")
    run_version = luigi.Parameter(default="")

    data_path = os.path.join(Config().config_paths['project_data'], 'consommation', 'besoins', 'climelioth')
    weather_path = os.path.join(Config().config_paths['project_data'], 'meteo')

    def requires(self):

        # Tasks inputs dict
        inputs = {
            'geometry': BuildGeometry(self.geometry_version),
            'weather': getWeather(self.weather_input)
        }

        # Versions of the inputs
        input_versions = {
            'geometry': self.geometry_version,
            'std_hyps': self.std_hyps_version,
            'weather': self.weather_input,
            'ProgrammeScaling': self.ProgrammeScaling,
            'ratios': self.ratios,
            'STDreplacement': self.STDreplacement,
            'run_version': self.run_version
        }

        # Test if an output already exists for this set of inputs
        exist, version = output_exists(self.data_path, inputs, input_versions)

        # Point to the existing folder if the output already exists or create a folder
        output_path = os.path.join(self.data_path, 'out', version)

        # Add the output folder creation as input task
        inputs['output_folder'] = CreateOutputDirectory(output_path, input_versions)

        return inputs

    def output(self):
        return luigi.LocalTarget(os.path.join(self.input()['output_folder'].path, "climelioth.parquet"))

    def run(self):
        
        if self.ProgrammeScaling == "True":
            programmepath = os.path.join(Config().config_paths['project_data'], 'consommation', 'geometrie', 'in', self.geometry_version, 'programme.xlsx')
        else:
            programmepath = ""
            
            
        if len(self.ratios) > 0:
            ratios = os.path.join(Config().config_paths['project_data'], 'consommation','besoins','ratios',self.ratios+".csv")
        else:
            ratios = ""
         
        if len(self.STDreplacement) > 0:
            STDpath = os.path.join(Config().config_paths['project_data'], 'consommation', 'besoins', 'STD', 'in', self.STDreplacement)
        else:
            STDpath = ""
            
        subprocess.call([
            'python',
            Config().config_paths['climelioth'],
            '-g', self.input()['geometry'].path,
            '-s', os.path.join(self.data_path, 'in', self.std_hyps_version),
            '-w', self.input()['weather'].path,
            '-r', ratios,
            '-p', programmepath,
            '-t', STDpath,
            '-o', self.output().path
        ])


class RunSolar(luigi.Task):

    geometry_version = luigi.Parameter()
    solar_parameters_version = luigi.Parameter()
    weather_input = luigi.Parameter(default="H1a")
    
    geometry_path = os.path.join(Config().config_paths['project_data'], 'consommation', 'geometrie')
    solar_path = os.path.join(Config().config_paths['project_data'], 'production', 'solaire_pv')
    weather_path = os.path.join(Config().config_paths['project_data'], 'meteo')

    def requires(self):

        # Tasks inputs dict
        inputs = {
            'geometry': BuildGeometry(self.geometry_version),
            'weather': getWeather(self.weather_input)
        }

        # Versions of the inputs
        input_versions = {
            'geometry': self.geometry_version,
            'solar_parameters': self.solar_parameters_version,
            'weather': self.weather_input
        }

        # Test if an output already exists for this set of inputs
        exist, version = output_exists(self.solar_path, inputs, input_versions)

        # Point to the existing folder if the output already exists or create a folder
        output_path = os.path.join(self.solar_path, 'out', version)

        # Add the output folder creation as input task
        inputs['output_folder'] = CreateOutputDirectory(output_path, input_versions)

        return inputs

    def output(self):
        return luigi.LocalTarget(os.path.join(self.input()['output_folder'].path, "solar.csv"))

    def run(self):

        subprocess.call([
            'python',
            Config().config_paths['solar'],
            '-g', self.input()['geometry'].path,
            '-p', os.path.join(self.solar_path, 'in', self.solar_parameters_version, 'solar_areas_modules.xlsx'),
            '-w', self.input()['weather'].path,
            '-o', self.output().path
        ])
        
    
class RunSmartgrid(luigi.Task):

    geometry_version = luigi.Parameter()
    std_hyps_version = luigi.Parameter()
    phasing_version = luigi.Parameter()
    weather_input = luigi.Parameter(default="H1a")
    ratios = luigi.Parameter(default="")
    
    ProgrammeScaling = luigi.Parameter(default="False")
    STDreplacement = luigi.Parameter(default="")
    
    solar_parameters_version = luigi.Parameter(default="")
    wind_power= luigi.Parameter(default="")
    
    network_geometry = luigi.Parameter(default="")
    network_temperatures = luigi.Parameter()
    temp_needs = luigi.Parameter(default="")
    
    systems = luigi.Parameter()
    run_years = luigi.Parameter(default='all')
    
    weight_monetary = luigi.Parameter(default='0')
    weight_ghg = luigi.Parameter(default='1')
    weight_energy = luigi.Parameter(default='0')

    costs = luigi.Parameter(default='')

    split_by_month = luigi.Parameter(default='True')  

    data_path = os.path.join(Config().config_paths['project_data'], 'reseau', 'puissances')

    def requires(self):

        # Tasks inputs dict
        inputs = {
            'climelioth': RunClimelioth(
                geometry_version = self.geometry_version,
                std_hyps_version = self.std_hyps_version,
                weather_input = self.weather_input,
                ProgrammeScaling = self.ProgrammeScaling,
                ratios = self.ratios,
                STDreplacement = self.STDreplacement
            ),
            'weather': getWeather(self.weather_input),
            'solar': RunSolar(
                geometry_version = self.geometry_version,
                solar_parameters_version = self.solar_parameters_version,
                weather_input = self.weather_input
            ),
            'network': makeNetwork(
                geometry_version = self.geometry_version,
                std_hyps_version = self.std_hyps_version,
                weather_input = self.weather_input,
                ProgrammeScaling = self.ProgrammeScaling,
                ratios = self.ratios,
                STDreplacement = self.STDreplacement
            )
        }

        # Versions of the inputs
        input_versions = {
            'geometry': self.geometry_version,
            'std_hyps': self.std_hyps_version,
            'phasing': self.phasing_version,
            'weather': self.weather_input,
            'wind_power': self.wind_power,
            'ProgrammeScaling': self.ProgrammeScaling,
            'ratios': self.ratios,
            'STDreplacement': self.STDreplacement,
            'solar_parameters': self.solar_parameters_version,
            'network_geometry': self.network_geometry,
            'network_temperatures': self.network_temperatures,
            'temp_needs': self.temp_needs,
            'systems': self.systems,
            'weight_energy': self.weight_energy,
            'weight_ghg': self.weight_ghg,
            'weight_monetary': self.weight_monetary,
            'costs': self.costs,
            'run_years': self.run_years,
            'split_by_month': self.split_by_month
        }

        # Test if an output already exists for this set of inputs
        exist, version = output_exists(self.data_path, inputs, input_versions)

        # Point to the existing folder if the output already exists or create a folder
        output_path = os.path.join(self.data_path, 'out', version)

        # Add the output folder creation as input task
        inputs['output_folder'] = CreateOutputDirectory(output_path, input_versions)

        return inputs

    def output(self):
        return luigi.LocalTarget(os.path.join(self.input()['output_folder'].path, "smartgrid_files.json"))

    def run(self):
        print("run smart grid")
        if self.network_geometry == "":
            network_geometry_path = ""
        else:
            network_geometry_path = os.path.join(self.input()['network'].path, self.network_temperatures, self.network_geometry + '.json')
        
        if self.solar_parameters_version == "":
            solar = ""
        else:
            solar = self.input()['solar'].path
            
        if self.wind_power == "":
            wind_power = ""
        else:
            wind_power = os.path.join(Config().config_paths['project_data'], 'production', 'wind_power', 'out', self.wind_power + ".csv")

        if self.temp_needs == "":
            temp_needs = ""
        else:
            temp_needs = os.path.join(Config().config_paths['project_data'], 'reseau', 'besoins_temperatures', self.temp_needs)
            
        subprocess.call([
            'python',
            Config().config_paths['smartgrid'],
            '--power', self.input()['climelioth'].path,
            '--phasing', os.path.join(Config().config_paths['project_data'], 'phasage', self.phasing_version, 'phasage.xlsx'),
            '--solar_pv_production', solar,
            '--wind_power', wind_power,
            '--weather', self.input()['weather'].path,
            '--network_geometry', network_geometry_path,
            '--systems', os.path.join(Config().config_paths['project_data'], 'production', 'systems', 'in', self.systems + ".csv"),
            '--temp_needs', temp_needs,            
            '--run_years', self.run_years,
            '--weight_energy', self.weight_energy,
            '--weight_ghg', self.weight_ghg,
            '--weight_monetary', self.weight_monetary,
            '--costs', os.path.join(Config().config_paths['project_data'], 'production', 'costs', 'in', self.costs + ".csv"),
            '--split_by_month', self.split_by_month,
            '--output_file_path', self.output().path
        ])

class makeNetwork(luigi.Task):

    geometry_version = luigi.Parameter()
    std_hyps_version = luigi.Parameter()
    weather_input = luigi.Parameter(default="H1a")
    ProgrammeScaling = luigi.Parameter(default="False")
    ratios = luigi.Parameter(default="")
    STDreplacement = luigi.Parameter(default="")
    
    network_version = luigi.Parameter(default="1")
    temperatures_version = luigi.Parameter(default="1")
    dim_factor = luigi.Parameter(default="1")

    data_path = os.path.join(Config().config_paths['project_data'], 'reseau','geometrie')




    def requires(self):

        # Tasks inputs dict
        inputs = {
            'climelioth': RunClimelioth(
                geometry_version = self.geometry_version,
                std_hyps_version = self.std_hyps_version,
                weather_input = self.weather_input,
                ProgrammeScaling = self.ProgrammeScaling,
                ratios = self.ratios,
                STDreplacement = self.STDreplacement
            ),
            'weather': getWeather(self.weather_input)
        }

        # Versions of the inputs
        input_versions = {
            'geometry': self.geometry_version,
            'std_hyps': self.std_hyps_version,
            'weather': self.weather_input,
            'ProgrammeScaling': self.ProgrammeScaling,
            'ratios': self.ratios,
            'STDreplacement': self.STDreplacement,
            'network_version': self.network_version,
            'temperatures_version': self.temperatures_version,
            'dim_factor': self.dim_factor        
        }

        # Test if an output already exists for this set of inputs
        exist, version = output_exists(self.data_path, inputs, input_versions)

        # Point to the existing folder if the output already exists or create a folder
        output_path = os.path.join(self.data_path, 'out', version)

        # Add the output folder creation as input task
        inputs['output_folder'] = CreateOutputDirectory(output_path, input_versions)

        return inputs

    def output(self):
        return luigi.LocalTarget(os.path.join(self.input()['output_folder'].path))

    def run(self):
        pathscenarios = os.listdir(os.path.join(Config().config_paths['project_data'], 'reseau', 'geometrie', 'in', self.network_version))
        temperaturescenarios = os.listdir(os.path.join(Config().config_paths['project_data'], 'reseau', 'temperatures', 'in', self.temperatures_version))
        temperaturescenarios = [tempscenario.split(".")[0] for tempscenario in temperaturescenarios]
        print("making Network")
         
        for tempscenario in temperaturescenarios:
            print("Temperature scenario:", tempscenario)
            os.mkdir(os.path.join(self.output().path,tempscenario))
            for pathscenario in pathscenarios:
                print("Network geometry scenario", pathscenario)
                subprocess.call([
                    'python',
                    Config().config_paths['geocad'],
                    '-p', self.input()['climelioth'].path,
                    '-n', os.path.join(Config().config_paths['project_data'], 'reseau', 'temperatures', 'in', self.temperatures_version, tempscenario+".json"),
                    '-g', os.path.join(Config().config_paths['project_data'], 'reseau','geometrie', 'in', self.network_version, pathscenario),
                    '-d', self.dim_factor,
                    '-o', os.path.join(self.output().path,tempscenario,pathscenario+".json")
                ])
    

class ExtractMetrics(luigi.Task):

    geometry_version = luigi.Parameter()
    std_hyps_version = luigi.Parameter()
    phasing_version = luigi.Parameter()
    weather_input = luigi.Parameter(default="H1a")
    ProgrammeScaling = luigi.Parameter(default="True")
    ratios = luigi.Parameter(default="")
    STDreplacement = luigi.Parameter(default="")  
    solar_parameters_version = luigi.Parameter()
    network_geometry = luigi.Parameter(default="")
    network_temperatures = luigi.Parameter()
    systems = luigi.Parameter()
    run_years = luigi.Parameter(default='all')

    data_path = os.path.join(Config().config_paths['project_data'], 'reseau', 'metrics')
    data_path = os.path.join(Config().config_paths['project_data'], 'reseau', 'metrics_new')

    def requires(self):

        # Tasks inputs dict
        inputs = {
            'smartgrid': RunSmartgrid(
                network_temperatures = self.network_temperatures,
                network_geometry = self.network_geometry,
                systems = self.systems,
                std_hyps_version = self.std_hyps_version,
                solar_parameters_version = self.solar_parameters_version,
                geometry_version = self.geometry_version,
                phasing_version = self.phasing_version,
                weather_input = self.weather_input,
                ProgrammeScaling = self.ProgrammeScaling,
                ratios = self.ratios,
                STDreplacement = self.STDreplacement,
                run_years = self.run_years
            )
        }

        # Versions of the inputs
        input_versions = {
            'network_temperatures': self.network_temperatures,
            'network_geometry': self.network_geometry,
            'systems': self.systems,
            'std_hyps_version': self.std_hyps_version,
            'solar_parameters_version': self.solar_parameters_version,
            'geometry_version': self.geometry_version,
            'phasing_version': self.phasing_version,
            'weather_input': self.weather_input,
            'ProgrammeScaling': self.ProgrammeScaling,
            'ratios': self.ratios,
            'STDreplacement': self.STDreplacement,
            'run_years': self.run_years
        }

        # Test if an output already exists for this set of inputs
        exist, version = output_exists(self.data_path, inputs, input_versions)

        # Point to the existing folder if the output already exists or create a folder
        output_path = os.path.join(self.data_path, 'out', version)

        # Add the output folder creation as input task
        inputs['output_folder'] = CreateOutputDirectory(output_path, input_versions)
        return inputs


    def output(self):
        return luigi.LocalTarget(os.path.join(self.input()['output_folder'].path, "extraction"))


    def run(self):

        import sys
        sys.path.append(Config().config_paths['smartgrid_performance'])
        import smartgrid_performance

        ef_file_path = os.path.join(Config().config_paths['project_data'], 'reseau', 'intensite_carbone', 'out', 'emissions_factors.csv')
        pef_file_path = os.path.join(Config().config_paths['project_data'], 'reseau', 'intensite_carbone', 'out', 'primary_energy_factors.csv')

        with open(self.input()['smartgrid'].path) as f:
            nc_paths = json.load(f)

        years = nc_paths.keys()

        for y in years:
            output_folder = os.path.join(os.path.dirname(nc_paths[y]), y)
            print(output_folder)

            if os.path.isdir(output_folder) == True:
                shutil.rmtree(output_folder)

            os.mkdir(output_folder)
            smartgrid_performance.export_metrics_to_excel(output_folder, nc_paths[y], ef_file_path, pef_file_path)

        open(self.output().path, 'a').close()

        



class BatchProcessing(luigi.WrapperTask):

    import pandas as pd

    scenarios = pd.read_csv(Config().module_path+"/data/scenarios.csv")

    def requires(self):
        for s in range(self.scenarios.shape[0]):
            yield ExtractMetrics(network_temperatures=str(self.scenarios.iloc[s]["network-temperatures"]),
                                network_geometry=str(self.scenarios.iloc[s]["network-geometry"]),
                                ProgrammeScaling=str(self.scenarios.iloc[s]["ProgrammeScaling"]),
                                ratios=str(self.scenarios.iloc[s]["ratios"]),
                                STDreplacement=str(self.scenarios.iloc[s]["STDreplacement"]),
                                systems=str(self.scenarios.iloc[s]["systems"]),
                                std_hyps_version=str(self.scenarios.iloc[s]["std-hyps-version"]),
                                solar_parameters_version =str(self.scenarios.iloc[s]["solar-parameters-version"]),
                                geometry_version=str(self.scenarios.iloc[s]["geometry-version"]),
                                phasing_version=str(self.scenarios.iloc[s]["phasing-version"]),
                                weather_input=str(self.scenarios.iloc[s]["weather-input"]),
                                run_years=str(self.scenarios.iloc[s]["run_years"]))
            



